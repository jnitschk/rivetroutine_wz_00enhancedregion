import os
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from GaudiSvc.GaudiSvcConf import THistSvc

theApp.EvtMax = -1
svcMgr.EventSelector.InputCollections = ["None"]
systWeights = []
job = AlgSequence()
svcMgr += THistSvc()


def safeFileName(name):
 name = name.strip()
 name = name.replace(".", "p").replace(" ", "_")
 name = name.replace("pyoda", ".yoda")
 name = name.replace(":", "_")
 return name


if systWeights is None: 
  systWeights = {'Nominal': 0}

analyses= "ANALYSIS"
rivet = Rivet_i()
for analysis in analyses.split(","):
  rivet.Analyses += [analysis]
rivet.RunName = ""
rivet.DoRootHistos = False
rivet.AnalysisPath = os.environ['PWD']
rivet.CrossSection = 1.0
rivet.SkipWeights = False
job += rivet

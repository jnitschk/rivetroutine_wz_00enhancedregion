# RivetRoutine_WZ_00enhancedRegion

```
source setupEnv.sh
rivet-build RivetAnalysis_ATLAS_WZ_00enhanced_emu.so ATLAS_WZ_00enhanced_emu.cc
athena RivetAnalysis_JO_Rivet3_364275_v1.py
```

## Grid jobs
If you want to send Grid jobs, take a look at the submissionTool.py script in PMGSystematicsTool: https://gitlab.cern.ch/atlas-physics/pmg/tools/systematics-tools/tree/master . 

1. Please follow the instructions here to setup the PMGSystematicsTool: https://gitlab.cern.ch/atlas-physics/pmg/tools/systematics-tools/-/blob/master/README.md
2. For setting up as given above:
```
ssh -Y <username>@lxplus.cern.ch 
source setupSystematicsTool.sh 
export PYTHONPATH=$PYTHONPATH:${LCG_RELEASE_BASE}/LCG_88b/MCGenerators/lhapdf/6.2.3/${LCG_PLATFORM}/lib/python2.7/site-packages
export PATH=$PATH:${LCG_RELEASE_BASE}/LCG_88b/MCGenerators/lhapdf/6.2.3/${LCG_PLATFORM}/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${LCG_RELEASE_BASE}/LCG_88b/MCGenerators/lhapdf/6.2.3/${LCG_PLATFORM}/lib
./submit_Sherpa.py
```
## Condor jobs (you might need to change this script)
For submitting condor jobs:

```
python create_condorjobs.py \
-input_dir <path to the directory with EVNT.root files> \
-condor_dir <path where you want to create the job submissions> \
-mc <name of the run related to the MC sample name, you can choose this.>
-noFilesPerJob <number of files you want to submit per job>
-rivet_routine <name of the rivet routine e.g. ATLAS_WZ_00enhanced_emu>
-jobOptions_py RivetAnalysis_JO_Rivet3_template.py
```

##For extracting the output of the condor jobs or the grid jobs (you might need to change this script):

You need PMGSystematicsTool for this (or the particular python script - yodamerge_tmp.py)

```
ssh -Y <username>@lxplus.cern.ch
source setupSystematicsTool.sh
export PYTHONPATH=$PYTHONPATH:${LCG_RELEASE_BASE}/LCG_88b/MCGenerators/lhapdf/6.2.3/${LCG_PLATFORM}/lib/python2.7/site-packages
export PATH=$PATH:${LCG_RELEASE_BASE}/LCG_88b/MCGenerators/lhapdf/6.2.3/${LCG_PLATFORM}/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${LCG_RELEASE_BASE}/LCG_88b/MCGenerators/lhapdf/6.2.3/${LCG_PLATFORM}/lib

python untar_yoda.py \
-input_dir <directory where *yodaXYZ.gz> \
-gen <generator e.g. Sherpa> \
-out_dir <directory where you want to store the combined output yoda file or the converted root file> \
-mc <the mc name you want to use for the root file e.g. Sherpa which gives you the root file Sherpa.root>
```

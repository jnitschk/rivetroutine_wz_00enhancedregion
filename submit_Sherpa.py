#!/usr/bin/env python
import os

#==============================================================================
# The first step in the workflow is to submit your samples.
# In this case, suppose you would like to submit an 'official' sample from
# an ATLAS MC production, but running on your very own, unvalidated
# RIVET routine 

# A good example would be running some ttbar MC through one of the unvalidated 
# ttbar rivet routines, which are available here:
# https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/Software/MCvalidation/Rivet/Rivet2.X/trunk/routines
# The one we are interested in, MC_TTbar_TruthSel, does not work out of the box
# from the svn repo above, so for the sake of illustration an working version
# is included:
# MC_TTbar_TruthSel.cc # Actual Rivet code
# MC_TTbar_TruthSel.info # Info about the routine
# MC_TTbar_TruthSel.plot # Plotting info for each plot
# Steering_TTbar_Truth # An additional steering file needed for the Routine

# The very first step is to compile the .cc. The tool expected an output of the for RivetAnalysis_<name>.so
# which can be obtained like so:
# os.system("rivet-buildplugin RivetAnalysis_MC_TTbar_TruthSel.so MC_TTbar_TruthSel.cc")

# Now, produce a list of input samples in a text file. See powheg_pythia8_ttbar_sample.txt as an example.

# To submit your samples, simply use the command below.
# you can just type the argument of os.system() on the command line...
# The example below has --nJobs 100 --nFilesPerJob 1, since it's just for a test/example. 
# If you wanted to run over the full sample, do not use the --nJobs/--nFilesPerJob arguments
# By default, the tool will check the DISD database for the list of available Matrix-element weights,
# and will submit one Rivet instance per weight. The DISD is extracted from the sample name assuming 
# it is called mc15_13TeV.<dsid>.<generator_name_and_details>_<process>_...
# which should always be true for official ATLAS samples
# the option --label/-l appends the specified label to your output dataset name
# the option --analysis/a is the name of the rivet analysis to run on.
# for unvalidated analyses you may need to compile the analysis.
# if, for sample is not in the DISD-weights database (in which case you should let your PMG contact know!!)
# or you don't want to use the DSID_weights database for whatever reason (eg custom sample..)
#, then you can use the option --downloadSample, which downloads a single file from
# the dataset, and checks explicitly what Matrix-element weights are available.
# then these weights are used to produce the Rivet instances in the job
# or --noSyst to run only on the nominal weight

# An important point is that in this case, you need an additional steering file to make this sample work properly !
# This can be added using --extraFiles Steering_TTbar_Truth (accepts comma-separated list)

os.system("submissionTool.py -i sample_Sherpa.txt -l v1 -a ATLAS_WZ_00enhanced_emu")
#os.system("submissionTool.py -i powheg_pythia8_ttbar_sample.txt -l ttbar_unvalRivet  -a MC_TTbar_TruthSel   --extraFiles Steering_TTbar_Truth --downloadSample ")
# os.system("submissionTool.py -i powheg_pythia8_ttbar_sample.txt -l ttbar_unvalRivet_ns  -a MC_TTbar_TruthSel --nJobs 100 --nFilesPerJob 1  --extraFiles Steering_TTbar_Truth --noSyst")

# Finally, othr useful options are:
# --dryRun : this will do everything except to actualyl submit the jobs.
# That way you can check your job options look sensible/have the right
# ME weights, and you could test them by running 'athena <job_option>.py'

